from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re
import csv
import time
from bs4 import BeautifulSoup



# Initializes the web engine, parses the data at request of site function, then exits
def open_url(url):
    driver = webdriver.Firefox()
    driver.implicitly_wait(30)
    driver.get(url)
    data = BeautifulSoup(driver.page_source, 'html.parser')
    driver.quit()
    return data


# Function for https://www.cardkingdom.com/
def card_kingdom(name):
    cardkingdomlist = []
    print('Searching Card Kingdom')
    data = open_url('https://www.cardkingdom.com/catalog/search?filter[ipp]=60&filter[sort]=name&filter[name]="' + str(name) + '"')
    search = data.find_all(class_='productCardWrapper')

    for x in search:
        if re.search('^[0]', x.find(class_='styleQty').text):
            continue
        list = []
        list.append(x.find(class_='productDetailTitle').text)
        set = x.find(class_='productDetailSet').text
        list.append(set.replace('\n', ''))
        price = x.find(class_='stylePrice').text
        list.append(price.replace('\n', ''))
        list.append(x.find(class_='styleQty').text)
        cardkingdomlist.append(list)
    return cardkingdomlist


def abu_games(name):
    abulist = []
    print('Searching ABUGames')
    data = open_url('https://abugames.com/magic-the-gathering/singles?search='+ str(name) + '&language=%5B%22English%22%5D&condition=%5B%22NM-M%22%5D&quantity=%5B1,9999%5D')
    search = data.find_all(class_='single-gallery-item')

    for x in search:
        list = []
        list.append(x.find(class_='title').text)
        list.append(x.find(class_='item-meta-data').text)
        list.append(x.find(class_='col-xs-2').text)
        list.append(x.find(class_='col-xs-2').find_next('div').text)
        if re.search(name.lower(), list[0].lower()):
            abulist.append(list)
    return abulist


def sc_games(name):
    sclist = []
    print('Searching SC Games')
    data = open_url('http://www.starcitygames.com/results?&lang%5B%5D=1&name=' + str(name) + '&namematch=AND&r_all=All&g%5BG1%5D=NM/M&sort1=4&available=1&sort2=1&sort3=10&numpage=25&display=1&switch_display=1')
    reg = data.find_all(class_='deckdbbody_row')
    foil = data.find_all(class_='deckdbbody2_row')

    for x in reg:
        if re.search('Supplies:', x.find(class_='search_results_2').find_next('a').text):
            continue
        else:
            list = []
            name = x.find(class_='card_popup').text
            list.append(name.replace('\n', ''))
            list.append(x.find(class_='search_results_2').find_next('a').text)
            list.append(x.find(class_='search_results_9').text)
            list.append(x.find(class_='search_results_8').text)
            sclist.append(list)

    for x in foil:
        if re.search('Supplies:', x.find(class_='search_results_2').find_next('a').text):
            continue
        else:
            list = []
            name = x.find(class_='card_popup').text
            list.append(name.replace('\n', ''))
            list.append(x.find(class_='search_results_2').find_next('a').text)
            list.append(x.find(class_='search_results_9').text)
            list.append(x.find(class_='search_results_8').text)
            sclist.append(list)
    return sclist


def mtg_mint(name):
    mmlist = []
    print('Searching MTGMint')
    data = open_url('http://www.mtgmintcard.com/mtg/singles/search?action=advanced_search&name=' + name + '&ed=0&lg_1=1&mo_1=1&mo_2=1&cc_1=1&cc_2=1&cc_4=1&cc_6=1&cc_7=1&cc_3=1&cc_8=1&cf=0&ct=-1&t2=&pf=-1&pt=99&tf=-1&tt=99&ra_4=1&ra_3=1&ra_2=1&ra_1=1&ra_6=1&rt=&prf=0.00&prt=-1&is=1')
    search = data.find_all("tr", {"id": "product_list_content"})

    for x in search:
        list = []
        list.append(x.find(class_='hidden-xs').findNext('td').text)
        if x.find(class_='text-center').find("img", recursive=False):
            list.append(x.find(class_='text-center').find("img", recursive=False).get('title'))
        else:
            list.append(x.find(class_='text-center').text)
        list.append(x.find_all(class_='text-right')[2].text)
        list.append(str(1))
        if re.search(name.lower(), list[0].lower()):
            mmlist.append(list)
    return mmlist


if __name__ == "__main__":
    card = input("Enter card: ")

    abu = abu_games(card)
    ck = card_kingdom(card)
    scg = sc_games(card)
    mtgm = mtg_mint(card)
    print(abu)
    print(ck)
    print(scg)
    print(mtgm)

    with open(r'cards/%s %s.csv' % (card, time.strftime("%Y%m%d-%H%M%S")), 'w', newline='') as csv_file:
        test = csv.writer(csv_file, delimiter=',')
        test.writerow(['abugames'])
        for x in abu:
            test.writerow(x)
        test.writerow(['cardkingdom'])
        for x in ck:
            test.writerow(x)
        test.writerow(['starcitygames'])
        for x in scg:
            test.writerow(x)
        test.writerow(['mtgmint'])
        for x in mtgm:
            test.writerow(x)

    csv_file.close()
